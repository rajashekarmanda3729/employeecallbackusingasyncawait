const { readAFile, retriveDataForIds, groupDataBasedOnCompanies,
    getDataFromCompnay, removeDetailsOfId, sortData, swapElementPositions,
    addNewKeyAndValueToData } = require('./solutionEmployeeCallBack.cjs')

async function performActionsOnData() {
    try {
        let data = await readAFile('data.json')
        data = JSON.parse(data)
        data = data.employees
        await retriveDataForIds([2,13,28],data)
        await groupDataBasedOnCompanies(data)
        await getDataFromCompnay(data)
        await removeDetailsOfId(2,data)
        await sortData(data)
        await swapElementPositions(data)
        await addNewKeyAndValueToData(data)
    } catch (err) {
        console.log(err)
    }
}

performActionsOnData()
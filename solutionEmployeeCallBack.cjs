const fs = require('fs')

function readAFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, 'utf8', (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}
function retriveDataForIds(employeeIds, data) {
    return new Promise((resolve, reject) => {
        employeeData = data
        let retriveDataForIds = employeeData.filter(eachEmployee => employeeIds.includes(eachEmployee.id))
        let jsonConvertedData
        try {
             jsonConvertedData = JSON.stringify(retriveDataForIds)
        }catch(err){
            console.log(err)
        }
        fs.writeFile('output/1-retriveDataForIds.json', jsonConvertedData, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(retriveDataForIds)
            }
        })
    })
}

function groupDataBasedOnCompanies(employeeData) {
    return new Promise((resolve, reject) => {
        let groupByCompanyObj = { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": [] }
        employeeData.map(eachEmployee => {
            switch (eachEmployee.company) {
                case 'Scooby Doo':
                    groupByCompanyObj['Scooby Doo'].push(eachEmployee)
                    break;
                case 'Powerpuff Brigade':
                    groupByCompanyObj['Powerpuff Brigade'].push(eachEmployee)
                    break;
                case 'X-Men':
                    groupByCompanyObj['X-Men'].push(eachEmployee)
                    break;
            }
        })
        let jsonConvertedData
        try {
             jsonConvertedData = JSON.stringify(employeeData)
        }catch(err){
            console.log(err)
        }
        fs.writeFile('output/2-groupByCompanyNames.json', jsonConvertedData, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(employeeData)
            }
        })
    })
}

function getDataFromCompnay(employeeData) {
    return new Promise((resolve, reject) => {
        let allDataForPowerPuffCompany = employeeData.filter(eachEmployee => eachEmployee.company === 'Powerpuff Brigade')
        let jsonConvertedData
        try {
             jsonConvertedData = JSON.stringify(allDataForPowerPuffCompany)
        }catch(err){
            console.log(err)
        }
        fs.writeFile('output/3-allDataofPowerpuffCompany.json', jsonConvertedData, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(allDataForPowerPuffCompany)
            }
        })
    })
}

function removeDetailsOfId(id, employeeData) {
    return new Promise((resolve, reject) => {
        let removedId2Data = employeeData.filter(eachEmployee => eachEmployee.id !== 2)
        let jsonConvertedData
        try {
             jsonConvertedData = JSON.stringify(removedId2Data)
        }catch(err){
            console.log(err)
        }
        fs.writeFile('output/4-removedId2Data.json',jsonConvertedData, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(removedId2Data)
            }
        })
    })
}

function sortData(employeeData) {
    return new Promise((resolve, reject) => {
        employeeData.sort((a, b) => {
            if (a.company > b.company) return 1
            else if (a.company < b.company) return -1
            else return (a.id > b.id) ? 1 : -1;
        })
        let jsonConvertedData
        try {
             jsonConvertedData = JSON.stringify(employeeData)
        }catch(err){
            console.log(err)
        }
        fs.writeFile('output/5-sortDataByCompanyAndId.json', jsonConvertedData, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(employeeData)
            }
        })
    })
}
function swapElementPositions(employeeData) {
    return new Promise((resolve, reject) => {
        function swapTwoElmentsOfArray(arr, elementIndex1, elementIndex2) {
            let firstElement = arr[elementIndex1]
            arr[elementIndex1] = arr[elementIndex2]
            arr[elementIndex2] = firstElement
            return arr
        }
        let index92 = null, index93 = null
        employeeData.map(each => {
            if (each.id == 92) index92 = employeeData.indexOf(each)
            if (each.id == 93) index93 = employeeData.indexOf(each)
        })
        let swapEmployeeData = swapTwoElmentsOfArray(employeeData, index92, index93)
        let jsonConvertedData
        try {
             jsonConvertedData = JSON.stringify(swapEmployeeData)
        }catch(err){
            console.log(err)
        }
        fs.writeFile('output/6-swapElementsonData.json', jsonConvertedData, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(swapEmployeeData)
            }
        })
    })
}

function addNewKeyAndValueToData(employeeData) {
    return new Promise((resolve, reject) => {
        let addBirthDayToData = employeeData.map(eachEmployee => {
            if (eachEmployee.id % 2 == 0) {
                return { ...eachEmployee, birthday: String(new Date()).slice(4, 15) }
            }
            return eachEmployee
        })
        let jsonConvertedData
        try {
             jsonConvertedData = JSON.stringify(addBirthDayToData)
        }catch(err){
            console.log(err)
        }
        fs.writeFile('output/7-addBirthdayToEvenIds.json', jsonConvertedData, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(addBirthDayToData)
            }
        })
    })
}
module.exports = {
    readAFile, retriveDataForIds, groupDataBasedOnCompanies,
    getDataFromCompnay, removeDetailsOfId, sortData, swapElementPositions, addNewKeyAndValueToData
}